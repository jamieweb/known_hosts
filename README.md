# known_hosts

## What is this for?

This is a repository that I use as an authoritative source for my SSH server key fingerprints (normally stored in `~/.ssh/known_hosts`). All of my devices that connect to my servers using SSH (e.g. my Ansible control machine, Raspberry Pi cluster, etc) can have their known_hosts updated easily by just pulling from this repository over HTTPS. The reason for creating this is so that when I rebuild my infrastructure (e.g. for major OS upgrades, such as Ubuntu 18.04 -> 18.10), I don't have to manually update the saved fingerprints for every device.

## How are fingerprints verified?

In situations where I can easily check the legitimate fingerprint directly on the origin server, I will do that and ensure that the fingerprints added to this repository match.

In situations where I cannot check the fingerprints directly on the server, I will check them from a separate machine to the one that I will be using to connect to the servers. This reduces the risk of any potential ongoing MitM attack that could result in me picking up bad fingerprints and then unknowingly connecting to the attacker's server.

## Updating SSH server key fingerprints automatically is a bad idea for security, so what is `update-known_hosts.sh` for?

Automatically updating the stored server key fingerprints without verifying them fundmentally breaks the security model of SSH, so that will never be done.

Instead, the script is just for automating the management and formatting of the known hosts files - it is still up to the human (myself) to perform the verification and then explictly accept any changes to the fingerprint.

## Is this repository supposed to be publicly accessible?

Yes, all of the information within this repository is intended to be publicly accessible.

## Configuration

Add the following to `~/.ssh/config`, ensuring that it applies to all relevant hosts:

    UserKnownHostsFile ~/.ssh/known_hosts /path/to/known_hosts/ldn01/fingerprint /path/to/known_hosts/nyc01/fingerprint
